﻿001	Northern Pride. Quartier général de la Garde sainte.
002	Quoi ?! Monzein est...	VO_ortima.ortima_battle_1028
003	Oui, malheureusement...
004	Quand le capitaine Yauras est arrivée, il était déjà aux portes de la mort...	VO_kanon_3s_cano.kanon_sys_0042
005	Ça veut dire que les renforts ne sont pas arrivés à temps ?	VO_kagura_3s_kagu.kagura_sys_0025
006	Oui. Il avait déjà subi des blessures fatales au moment où je suis arrivée.
007	Non...	VO_kagura_3s_kagu.kagura_sys_0022
008	C'est un peu décevant, tout ça...	VO_ortima.ortima_sys_0022
009	Quoi ?!
010	Ça manque de touche finale pour le capitaine Yauras. Comment dire... ? C'est comme une blague sans chute.	VO_ortima.ortima_sys_0025
011	Je voudrais bien ne pas y croire, mais c'est la vérité. C'est pénible à accepter, mais nous n’avons pas le choix.	VO_yaurasu.yaurasu_sys_0035
012	Un vétéran, un gros dur comme Monzein... N'est désormais plus de ce monde...	VO_ortima.ortima_sys_0012
013	Et je suis censé l'accepter ? Haha... Ne me faites pas rire.
014	Othima...
015	Moi je refuse d'y croire. Il est bien trop récalcitrant pour mourir aussi facilement.	VO_ortima.ortima_chara_1005
016	En plus il ne m'a toujours pas payé le repas qu'il me devait.
017	Non mais franchement... Jamais je n'accepterai quelque chose d'aussi ridicule !	VO_ortima.ortima_sys_0015
018	Capitaine Othima...
019	Pardon, c'est de ma faute... S'il n'avait pas essayé de me protéger, le capitaine Monzein serait encore...	VO_basini.basini_sys_1019
020	Non.	VO_yaurasu.yaurasu_sys_0016
021	Monzein est mort en utilisant son sceau brûlant. Il a suivi son code de chevalerie.
022	Tout ce qui s'est passé n'est que le résultat de ses propres choix. Tu ne peux pas t'en vouloir pour ce qui s'est passé, Basheeny.
023	Mais...	VO_basini.basini_sys_0018
024	L'heure n'est plus au deuil.	VO_yaurasu.yaurasu_sys_0012
025	Monzein a donné sa vie pour notre avenir à tous.
026	Si vous sombrez dans le chagrin, vous gâcheriez son sacrifice.
027	...
028	Capitaine Othima... C'est aussi très dur pour moi d'accepter la mort du capitaine Monzein.
029	Après tout, c'est lui qui m'a appris ce que cela voulait vraiment dire d'être un chevalier... Mais...
030	Oui, je sais...	VO_ortima.ortima_sys_0018
031	C'est juste que je ne suis pas encore prêt à accepter ce qui s'est passé pour l'instant.
032	Je m'excuse... Donnez-moi encore un peu de temps pour me défouler...	VO_ortima.ortima_battle_1013
033	Entendu...
034	Nous revoilà, capitaine.
035	Suzuka, Kazahaya ! Que se passe-t-il ?
036	Mes excuses... Nous avons fouillé la bibliothèque de fond en comble...	VO_kazahaya.kazahaya_sys_0035
037	Mais nous n'avons absolument rien trouvé sur les templiers de Veda.
038	Je vois. Je me demande si c'est une nouvelle organisation...	VO_kanon_3s_cano.kanon_sys_0031
039	Ils étaient assez puissants pour tuer le capitaine Monzein... Et même Freed Dike semble se méfier de ce groupe.
040	On peut donc facilement en conclure que ses membres possèdent une puissance considérable... Mais qui sont-ils ?
041	C'est une organisation de l'ombre qui vit dans les zones les plus secrètes du monde.
042	Ils viennent tous d'endroits différents et gardent leurs présences cachées. Il n'y a évidemment aucun document sur eux.
043	Mais comment sais-tu tout ceci ?	VO_yaurasu.yaurasu_sys_1047
044	Je pense que l'organisation en elle-même est originaire de Lost Blue...
045	Lost Blue ?
046	C'est un endroit très peu connu, uniquement réservé aux parias, à ceux qui ont été chassés duurs royaumes ou tribus respectives...
047	C'est un peu une zone de non-droit.
048	Elle devrait se trouver sous Slothstein.
049	Une zone de non-droit où tous les parias se réunissent ?	VO_caida.caida_sys_1025
050	Oui. Lost Blue est comme un abysse que la lumière n'atteint pas et où l'air reste perpétuellement vicié.
051	Cette ville que nous avons rencontrée à Lustburg, cette Acht... Elle a dit qu'elle venait de là-bas.
052	Il n'existe aucune information sur cet endroit dans tous les documents que nous avons réuni du monde entier...
053	Comment peux-tu connaître cet endroit, Othima ?	VO_yaurasu.yaurasu_sys_0013
054	C'est parce qu'apparemment le capitaine Othima vient lui aussi de Lost Blue...	VO_kagura_3s_kagu.kagura_sys_1034
055	Quoi ??
056	Je ne viens pas vraiment de là-bas, mais j'y ai fait un petit séjour quand j'étais adolescent, oui...	VO_ortima.ortima_sys_0022
057	J'avais été exilé de Lustburg et j'ai bourlingué de royaume en royaume jusqu'à me retrouver à Lost Blue.
058	Est-ce que ça veut dire que tu connais ces templiers ?	VO_yaurasu.yaurasu_sys_1047
059	Pas vraiment, non. Je n'ai passé que très peu de temps là-bas.
060	Après tout il y avait beaucoup d'allées et venues de gens à la recherche de Veda...
061	Je me souviens avoir entendu beaucoup de gens parler de ça sans jamais arriver à quoi que ce soit de concret...
062	Bref, c'est probablement l'organisation la plus dangereuse du monde.
063	Veda... Acht a prononcé ce mot, elle aussi. Que veut-il dire ?
064	Je ne sais pas exactement comment le décrire...	VO_ortima.ortima_sys_0031
065	Lost Blue est censé contenir une tour invisible qui mène à une autre dimension.
066	Une tour vers une autre dimension ?!	VO_kanon_3s_cano.kanon_sys_0026
067	C'est vraiment difficile à expliquer, mais...	VO_ortima.ortima_chara_1011
068	C'est probablement formé d'une puissante énergie, probablement alchimique. C'est une tour qui n'existe pas sous forme tangible.
069	On l'appelle la tour mystique Veda.
070	La tour mystique Veda ?	VO_yaurasu.yaurasu_sys_0017
071	Oui. C'est une tour très mystérieuse. Quand on y va, certains de nos sens peuvent tout simplement changer.
072	Ils deviennent plus aiguisés, en un certain sens.
073	Mais que vont faire les gens à cette tour ?
074	Les raisons peuvent varier... Certains y vont parce qu'ils veulent abandonner ou oublier quelque chose.
075	Il y a également des surnaturalistes qui ont transcendé un état allant au-delà des connaissances humaines. Tous sont fascinés par cette tour.
076	Donc si je résume, Capitaine Othima, après votre départ de la ville...
077	Les habitants de Lost Blue se sont réunis pour former une bande de mercenaires...
078	Est-ce bien cela ?
079	Qui sait ? À l'origine, les gens rassemblés à Lost Blue n'avaient aucun objectif en particulier et encore moins l'intention de s'entraider.	VO_ortima.ortima_sys_0031
080	Cela dit... Ils ont effectivement travaillé ensemble comme bandits afin de subvenir à leurs besoins...
081	Certains de ces groupes auraient donc tout simplement pu passer à l'échelon supérieur...	VO_yaurasu.yaurasu_sys_0045
082	C'est parfaitement envisageable, oui.
083	J'ai même entendu dire que certains habitants de Lost Blue travaillaient comme assassins.
084	Je n'arrive pas à croire qu'il existe un endroit servant de refuge aux criminels, comme ça...	VO_kanon_3s_cano.kanon_sys_0021
085	Un instant, je ne peux pas te laisser dire ça.	VO_ortima.ortima_chara_1002
086	Comme je l'ai expliqué, des parias et autres exilés du monde entier se réunissent à Lost Blue.
087	Parmi eux, certains étaient tout simplement des gens un peu perdus. Pas des criminels.
088	Certains ont même été aidés par cet endroit... Comme moi, par exemple.
089	Tu veux dire que c'est aussi un endroit où les âmes perdues peuvent trouver le salut...	VO_yaurasu.yaurasu_sys_0016
090	Oui, exactement. Quoi qu'il en soit, c'est un endroit rempli de gens aux capacités guerrières avancées.
091	Je pense qu'il va falloir entièrement revoir notre stratégie afin de nous adapter à ce nouveau danger.
092	Cet endroit se trouve en dessous de Slothstein, c'est bien ça ?	VO_kanon_3s_cano.kanon_sys_0031
093	Ce serait probablement une perte de temps d'y envoyer des soldats.
094	Ils semblaient connaître mon existence, ce qui veut dire qu'ils doivent se douter que je vais vous parler de Lost Blue.
095	Au mieux en y allant nous n'apprendrions donc rien, et au pire nous tomberions dans un piège...	VO_yaurasu.yaurasu_sys_0017
096	Les habitants de Lost Blue ne sont pas vraiment du genre à s'entraider, mais ils ont un grand respect pour les liens qui unissent tous ceux qui vivent là-bas.
097	Donc même si on y allait et qu'on interrogeait quelqu'un...
098	Il n'existe aucune trace de ces templiers, donc on ne pourrait pas vraiment apprendre quoi que ce soit d'exploitable.
099	Y envoyer des soldats serait donc tout simplement une perte de ressources. Bon.	VO_caida.caida_sys_0030
100	Il vaut mieux se focaliser sur les mouvements des troupes de Greed Dike plutôt que de partir chasser un ennemi caché.
101	Oui. De toute façon ces templiers devraient suivre l'armée de Greed Dike dans sa recherche des pierres sacrées.
102	Je pense que nous devrions prendre des mesures aussi tôt que possible pour protéger les royaumes qui n'ont pas encore été attaqués.
103	Nous n'avons pas pu empêcher le vol de la pierre sacrée Asmodée de Lustburg, mais...	VO_kagura_3s_kagu.kagura_sys_0005
104	Le Leviathan d'Envylia a été rendu totalement inutile.
105	Leur prochain objectif va être Satna, la pierre sacrée de Wratharis.
106	Ou bien Belphégor, la pierre de Slothstein ?
107	Et si jamais ils ont entendu parler du quartet de cristal de Saga, ils pourraient bien tenter de l'obtenir aussi...	VO_caida.caida_sys_0031
108	Nous n'avons donc aucune idée du prochain royaume qu'ils vont envahir.	VO_yaurasu.yaurasu_sys_0017
109	Mais je pense qu'ils ne viendront pas ici, à Northern Pride...
110	Pourquoi ?	VO_kagura_3s_kagu.kagura_sys_1047
111	Si j'en crois ce qu'a dit le capitaine Kudan...	VO_kanon_3s_cano.kanon_sys_0031
112	Le but d'Orion est d'obtenir la même source d'alchimie que Nimul utilisait.
113	On pourrait donc en conclure que la destination finale de l'armée de Greed Dike...
114	Sera la Tour de Babel, supposément fabriquée par Nimul lui-même, qui se trouve au centre de Northern Pride.
115	Tu as raison. Et si c'est le cas, alors le prochain royaume où Orion va se rendre...	VO_yaurasu.yaurasu_sys_0045
116	N'est ni Wratharis ni Slothstein, dont l'emplacement des pierres sacrées reste incertain, mais...
117	Saga... ?!	VO_caida.caida_sys_0027
118	C'est possible... Avant je ne me serai pas laissé aller à ce genre de suppositions, mais...	VO_ortima.ortima_sys_0031
119	Après tout il était à Envylia en chair et en os...
120	Orion est tout de même connu pour mener ses troupes et combattre au front directement.	VO_kagura_3s_kagu.kagura_sys_1047
121	C'est pour ça que j'ai cru qu'il allait tenter de nous prendre par surprise en agissant de manière contre-intuitive... Mais il ne l'a pas fait.
122	Orion, de manière contre-intuitive ??	VO_kagura_3s_kagu.kagura_chara_1013
123	Orion donne l'image de quelqu'un d'impulsif et d'irréfléchi...
124	Mais je pense qu'au fond c'est un homme très calme et surtout un excellent tacticien.
125	Quoi ? Un tacticien ?	VO_kagura_3s_kagu.kagura_sys_0025
126	C'est également l'impression que j'ai eue en le rencontrant face à face.	VO_yaurasu.yaurasu_sys_0004
127	Qu'est-ce que tu en dis, toi qui es aussi un fin tacticien ?	VO_yaurasu.yaurasu_chara_0009
128	Pourquoi crois-tu que cette fois-ci il a choisi une approche directe plutôt que d'essayer de nous surprendre ?
129	Probablement parce qu'il avait quelque chose dans sa manche qui lui permettait d'avoir l'avantage...
130	Les templiers de Veda !	VO_kanon_3s_cano.kanon_sys_0042
131	Exact. Un plan conçu par un stratège peut souvent être percé à jour par un autre.
132	Orion le sait très bien et il a donc tout fait pour introduire un élément de chaos dans la bataille.
133	Franchement, je ne peux absolument pas prévoir ce que vont faire ces templiers de Veda.
134	Impossible... Tu es sûr de ne pas te sous-estimer ?	VO_kagura_3s_kagu.kagura_sys_0022
135	Non, pas le moins du monde...
136	Orion a beau être très excentrique, c'est aussi un immense tacticien, à un niveau totalement différent du mien.
137	Je vois...	VO_kanon_3s_cano.kanon_sys_0042
138	Il est donc probable qu'il décide de se lancer à l'attaque de royaumes auxquels nous ne penserions jamais.
139	Prendre une approche défensive n'est probablement pas la meilleure des solutions dans le cas présent. Je pense qu'il faut au contraire passer à l'offensive.
140	Entendu. Capitaine Othima, tu vas aller à Wratharis avec Basheeny.	VO_kanon_3s_cano.kanon_sys_0037
141	Et moi ?	VO_kagura_3s_kagu.kagura_sys_0001
142	Rends-toi à Saga le plus vite possible.	VO_kanon_3s_cano.kanon_sys_0004
143	Seida, quant à elle, va rester à Northern Pride afin de pouvoir utiliser sa capacité à lire le vent.
144	J'imagine que ça veut dire que je vais aller à Slothstein, alors ?	VO_yaurasu.yaurasu_sys_0016
145	Absolument. Si tu n'y rencontres aucun problème particulier, tu peux aller à Saga et retrouver Kagura.
146	Kagura, Yauras... Je vous confie le royaume de Saga.	VO_caida.caida_sys_0035
147	Tu peux compter sur nous, on s'en occupe. Bien, on a tous nos ordres, alors partons sur-le-champ !	VO_kagura_3s_kagu.kagura_sys_0004
148	Attendez ! Avant de partir, promettez-moi tous une chose...	VO_kanon_3s_cano.kanon_sys_0018
149	Oui ?
150	Promettez-moi de revenir ici en vie, je vous en prie.	VO_kanon_3s_cano.kanon_sys_0004
151	Oh ça va, pas la peine de dramatiser comme ça...	VO_yaurasu.yaurasu_sys_0012
152	Elle ne dramatise pas, après ce qui est arrivé à Monzein, c'est normal d'être inquiète...
153	Je pense qu'il serait même prudent d'en profiter pour tous nous reconcentrer et focaliser nos esprits...
154	Nous devons reconnaître la force de notre adversaire... Et une petite promesse ne nous fera pas de mal !	VO_ortima.ortima_sys_0004
155	Oui... Levons tous nos armes.	VO_kagura_3s_kagu.kagura_sys_0005
156	Entendu.	VO_basini.basini_sys_0004
157	J'aime pas ça, mais puisqu'il le faut...	VO_yaurasu.yaurasu_sys_0010
158	En général j'enfouis ce genre de sentiments tout au fond de moi, d'habitude...	VO_yaurasu.yaurasu_sys_0016
159	Je... Je vais accepter la détermination brute de Monzein.
160	Je promets d'empêcher toute autre avancée de l'armée de Greed Dike...	VO_yaurasu.yaurasu_sys_0017
161	Moi aussi ! Je ne laisserai pas la mort du capitaine Monzein être vaine !	VO_basini.basini_sys_1019
162	L'âme du capitaine Monzein continuera de toute façon à vivre en chacun de nous !	VO_caida.caida_sys_0004
163	Il nous accompagnera toujours, lui comme son code de chevalerie !
164	Imaginez un peu ce qu'il dirait si jamais je me faisais tuer par l'armée de Greed Dike...
165	Je promets de revenir en vie, voilà.
166	Merci à tous ! Pour le capitaine Zain et pour le capitaine Monzein...	VO_kanon_3s_cano.kanon_battle_0018
167	Nous allons mettre un terme au plan maléfique du Monarque fauve !!
168	La Garde sainte a réussi à repousser l'armée de Greed Dike des royaumes d'Envylia et de Lustburg.
169	Mais la pierre sacrée Leviathan a perdu son pouvoir, et le capitaine Monzein, de la 2e division, est décédé.
170	Après tant de sacrifices, il est difficile pour les membres de la Garde de sourire.
171	Pendant ce temps, la menace de Greed Dike continue de grandir, menée par la détermination d'un seul homme.
172	Ce danger restera connu dans l'Histoire comme le plus grand ayant jamais pesé sur le continent de Babel.
173	Ceci est le souvenir d'un roi dément surnommé le Monarque fauve... Et de l'immense guerre qui changea à jamais les lois de ce monde.
174	THE ALCHEMIST CODE
175	Souvenirs de la pierre sacrée : Le destin du monarque fauve
176	Nous allons bientôt pénétrer dans le territoire de Wratharis. Faites attention à vous.
177	Entendu. Capitaine Othima, vous croyez vraiment que Greed Dike va débarquer ici ?	VO_basini.basini_sys_0005
178	C'est ce qu'on va voir... Mais au fait, j'ai une question importante à te poser.	VO_ortima.ortima_sys_0031
179	Est-ce à propos de la bataille d'Envylia.	VO_basini.basini_sys_0006
180	Non... C'est quoi ton plat préféré ?	VO_ortima.ortima_chara_1011
181	Mon plat préféré ? Hein ?	VO_basini.basini_sys_0012
182	C'est une question très importante. Plus tu es spécifique, mieux c'est. Alors ?
183	Eh bien, euh... Voyons voir... C'est probablement le ragoût de champignons de Melpoto.	VO_basini.basini_sys_0025
184	Je vois. Bien. Je retiens. Je t'en préparerai un à l'occasion.	VO_ortima.ortima_sys_0044
185	Ah bon ? Eh bien... Merci. Mais... Pourquoi cette question ?	VO_basini.basini_sys_0006
186	C'est juste que je ne connais rien du tout de ta vie privée alors je voulais connaître un peu tes goûts.	VO_ortima.ortima_chara_1011
187	Quoi ?
188	Je veux devenir plus ami avec toi. Depuis que ce bon vieux Monzein est mort, je suis le seul capitaine homme qui reste...	VO_ortima.ortima_sys_0020
189	Ah...
190	Tous les autres capitaines sont des femmes très fortes de caractère... Alors je me suis dit que tu te sentirais un peu seul.
191	Greed Dike nous a vraiment mis dans l'embarras à plus d'un titre.	VO_ortima.ortima_sys_0030
192	Oui. Tout est de la faute d'Orion et il va devoir le payer très cher.	VO_basini.basini_sys_0004
193	Exactement. On va l'éclater !
194	Je vous aiderai du mieux que je peux. Je veux venger le général à tout prix...
195	Le général ?	VO_basini.basini_sys_0006
196	Je parle du capitaine Zain... C'est Orion qui a corrompu l'esprit de Kudanstein... Et qui l'a poussé à tuer Zain.	VO_kazahaya.kazahaya_battle_0022
197	Zain est le général qui m'a protégée et... Ils... Ils l'ont... Je jure du venger à tout prix !
198	Parfait. Ta présence est comme un phare dans une nuit sans lune. Tu es indispensable. Je compte sur toi, Kazahaya.
199	Merci... Mais, capitaine Othima, il y a une chose que je ne comprends pas.	VO_kazahaya.kazahaya_sys_0037
200	Oui ?
201	Greed Dike a essayé de voler la pierre sacrée d'Envylia sans relâche, mais...	VO_kazahaya.kazahaya_sys_0031
202	Pourquoi ont-ils abandonné si facilement dans le cas de la pierre sacrée de Lustburg ?
203	Cela me paraît difficile à croire qu'ils n'auraient pas tout fait pour essayer de la récupérer.
204	Je pense que la réponse se situe dans le fameux équilibre du monde...
205	L'équilibre du monde ?	VO_kazahaya.kazahaya_sys_0025
206	Oui. Briser l'équilibre de ce monde permet de faire apparaître la source d'alchimie.
207	C'est ce que croit Orion. Son but n'est donc pas de récupérer toutes les pierres sacrées.
208	Tant qu'il peut déséquilibrer le monde, son objectif sera rempli...
209	Je vois...	VO_kazahaya.kazahaya_sys_0044
210	La pierre sacrée Asmodée de Lustburg est gardée dans un endroit très spécial.
211	Elle est cachée très haut dans le ciel, ce qui la rend pratiquement inaccessible.
212	Orion en a peut-être conclu qu'il n'avait pas de temps à perdre à essayer de l'obtenir.
213	Il n'essayera donc que d'obtenir ce qui est vraiment à portée de main ?	VO_basini.basini_sys_0025
214	Je pense, oui. L'invasion de Lustburg n'était qu'une démonstration de puissance, au fond.
215	Il voulait certainement juste faire se disperser la Garde sainte.
216	Il voulait également attirer Seida à lui, car il sait qu'elle possède probablement la pierre sacrée de Gluttony Foss. Je pense que c'est ça, son véritable objectif.
217	Je vois...	VO_basini.basini_sys_0045
218	Cela veut donc dire qu'il n'envahira probablement pas Wratharis ou Slothstein car l'emplacement duurs pierres sacrées est inconnu.
219	Précisément.	VO_ortima.ortima_sys_0004
220	La pierre sacrée qu'il peut être sûr et certain d'obtenir est donc à Northern Pride.	VO_basini.basini_sys_0018
221	Ou bien, si les informations pertinentes sont parvenues à ses oreilles, les cristaux de Saga...
222	Mais il se dirige donc vers l'un ou l'autre royaume, n'est-ce pas ?
223	Alors pourquoi avons-nous également envoyé des soldats à Wratharis et à Slothstein ? Ne devrait-on pas tous aller à Saga ?	VO_kazahaya.kazahaya_sys_0031
224	Sur le papier nos actions ont effectivement l'air d'être du gâchis de ressources.
225	Mais Orion semble aimer jouer à des petits jeux avec nous.
226	Quoi ?	VO_kazahaya.kazahaya_sys_0025
227	Attention... Je crois qu'on a des invités, et ils n'ont pas l'air d'être là pour discuter.
228	Des soldats de Greed Dike ?! Mais depuis quand sont-ils là ?	VO_basini.basini_sys_0025
229	Qui sait ? Mais ils vont voir... Je vais les punir de gâcher ma pause-déjeuner.	VO_ortima.ortima_battle_0030