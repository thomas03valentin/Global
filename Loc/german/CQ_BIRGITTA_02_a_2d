﻿001	Für's Erste sind sie besiegt, aber sie kommen sicher wieder. Was willst du machen, Mirianne?
002	Hat einer von euch eine Idee, was diese Bestien anziehen könnte?<br>
003	Der neue Händler, der die Mine übernommen hat, nutzt ein paar aufwendige und ziemlich zerstörerische Bergbaumethoden. Ich glaube, seitdem gibt es eine zunehmende Anzahl an Bestien.
004	Man könnte meinen, die Biester kommen hierher, weil sie aus ihrem Habitat vertrieben werden.
005	Das ist ja furchtbar! Hat jemand von euch den Händler konfrontiert und versucht, ihn von seinen Methoden abzuhalten?
006	Ja, schon, aber...
007	Das ist eine der profitabelsten Gemmen-Minen auf dem ganzen Kontinent. Ich verstehe schon, warum der Händler sie nicht aufgeben will.
008	Ja, das stimmt schon. Unsere Bitten werden ignoriert. Jetzt können wir gegen die Bestien höchstens einen Zaun errichten.
009	Aber leider haben wir das bisher nicht geschafft, weil wir einfach zu oft angegriffen werden.
010	Wenn Sie als Schamanin die Bestien abhalten könnten, während wir einen Abwehrzaun bauen...
011	Ach so, deshalb haben Sie sich so über mein Kommen gefreut. Das sollte kein Problem sein. Ich kümmere mich darum.
012	Auch ohne eine Bedrohung durch Bestien ist es eine gute Idee für jeden Ort, einen Abwehrzaun zu haben.
013	Soll ich helfen? Ich nicht ganz unfähig, müssen Sie wissen.
014	Oh, gerne! Das war im letzten Kampf deutlich zu sehen.
015	Mit zwei Bewachern von diesem Kaliber haben wir nichts zu befürchten!
016	Ist schon gut, über den Zaun haben wir genug geredet. Was wissen Sie über den Händler?
017	Hm? Wieso interessiert dich das so?
018	Weil der Händler die eigentliche Ursache ist. Jetzt, wo wir das wissen, haben wir einen Plan. Also führen wir ihn aus.
019	Wie du meinst. Das ist nicht mein Fachgebiet, das überlasse ich dir.<br>
020	Mirianne versteht es einfach nicht. Man tut sich keinen Gefallen, wenn man sich in die Probleme anderer einmischt.
021	Hilft man einem, leidet ein anderer. Es sind nie alle glücklich. Wozu soll man es überhaupt versuchen?
022	Am Ende passiert ihr selbst noch etwas. Vielleicht sollte ich sie besser abhalten... Hmmm, nein, dann mische ich mich selbst zu sehr ein.
023	Heda! Hallo!
024	Pah, Kinder heutzutage sind einfach respektlos. Was ist denn, Kind?<br>
025	Redest du mit den Erwachsenen? Spiel lieber mit mir!
026	Na, du hast ja ganz schön lebhaft.
027	Weißt du nicht, dass es gefährlich ist, mit Fremden zu reden?
028	Was, wenn ich eine böse Hexe bin, die sich ins Dorf geschlichen hat, um euch zu stehlen und zu fressen?
029	...
030	Hahahahaha! Sie sind aber komisch! Eine böse Hexe sagt doch niemandem, dass sie eine böse Hexe ist!
031	Hmpf.
032	Ja! Außerdem helfen Sie doch den Erwachsenen mit irgendwas. Dann können Sie keine böse Hexe sein.
033	Hmm. Hahaha! Ich seh schon, ihr seid mir beide viel zu schlau.
034	Spielen wir jetzt, oder was?
035	Klar. Aber besonders gut kenne ich mich mit Kinderspielen nicht aus. Was spielen wir? Fangen?
036	Das können wir nicht. Dann schimpft die Mama.
037	Wieso sollte sie da schimpfen? Spielen Kinder nicht fangen? Etwas Bewegung tut doch gut.
038	Mama sagt, wir müssen drinnen bleiben und ruhig sein, weil es so viele Bestien gibt.
039	Oh. Verstehe.<br>
040	Drinnen ist es so langweilig! Ich will lieber draußen Fangen oder Verstecken spielen!<br>
041	Sie helfen mit dem Zaun, oder? Sie bekämpfen die Bestien?
042	Und dann können wir wieder draußen spielen, stimmt's? Wenn die bösen Bestien weg sind?
043	Böse Bestien... Habt ihr euch schon mal gefragt, warum die Bestien überhaupt da sind?
044	Hä?
045	Bestien findet man normalerweise nicht grundlos dort, wo Menschen sind. Darüber müsst ihr mal nachdenken, wenn ihr das eigentliche Problem lösen wollt.
046	Versteh' ich nicht, aber Mama und Papa sagen, Bestien sind böse.
047	Und wir dürfen erst wieder draußen spielen, wenn sie weg sind.
048	Sie machen sich nur Sorgen um euch.
049	Es ist schon gruselig, wenn man nie weiß, wann die Bestien wieder angreifen.<br>
050	Das kann ich mir vorstellen. Wenn der Zaun fertig ist, kommen sie nicht mehr herein, also habt keine Angst. Jetzt solltet ihr aber schnell nach Hause gehen, bevor es dunkel wird.
051	Menno. Na gut.
052	Böse Bestien, was?
053	Aber traurig ist es schon, dass sie nicht mehr draußen spielen können.
054	Hehe, spielst du mit den Kindern?
055	Ja, so ähnlich. Bist du fertig mit deiner Unterhaltung?
056	Ja. Ich habe erfahren, was ich wissen wollte.
057	Als Gemmen-Händlerin findest du diese zerstörerische Art des Bergbaus sicher untragbar.
058	Ich habe schon von Händlern gehört, die es zu weit getrieben und Minen zerstört haben. Die Natur lässt sich manchmal nur schwer richtig beeinflussen.
059	Ich finde, für angemessenen Profit ist es gut, so weit zu gehen, wie man nur kann.<br>
060	Je mehr Gemmen in der Mine, desto bessere Gemmen sehen wir auf dem Markt. Aber trotzdem...
061	Ja? Spuck's aus, du bist doch sonst auch so direkt.
062	Hier geht es nicht mehr nur um Profit. Diese Nach-mir-die-Sintflut-Einstellung kann ich einfach nicht gutheißen.
063	Mich interessiert das ganze Geschäft wenig, ich halte mich dabei heraus. Bis morgen.
064	Eine Sache wollte ich aber noch fragen. Machst du das öfter? Dich in die Probleme anderer Leute einzumischen?
065	Nein, immer mache ich das nicht. Aber wenn ich kann, helfe ich gerne so gut ich kann.
066	So gut du kannst...
067	Es ist nicht ungewöhnlich, Menschen in Not helfen zu wollen. Oder siehst du das anders?
068	Nein, natürlich nicht. Ich denke nur, es ist eine schlechte Idee, sich einzumischen.
069	Machst du dir etwa Sorgen um mich? Wie süß.<br>
070	Süß?! I-ich spreche nur aus Erfahrung und versuche, dich gut zu beraten!
071	Meine Meinung änderst du trotzdem nicht. Mein Motto ist: Adel verpflichtet.
072	Adel... was?
073	Einfach gesagt... folgt aus großer Macht große Verantwortung.
074	Du meinst, wenn du nicht hilfst, dann ist dein Motto sozusagen wertlos?
075	So ist es!