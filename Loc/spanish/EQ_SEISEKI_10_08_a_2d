﻿001	Región de Saga. Cerca del templo de viento.
002	Así que este es el asentamiento de la tribu del viento, donde nos aguarda la última Materia.	VO_sol.sol_sys_0044
003	¿Qué es esto? No parece haber signos de vida humana.	VO_sol.sol_sys_0031
004	... ...
005	¿No es el momento de que digas algo? No me importará quemar todo este paisaje si me hace falta, ¿sabes?	VO_sol.sol_sys_0002
006	¿Estás tonto? Con todo el escándalo que vas haciendo, cualquiera se olería enseguida lo que está pasando.	VO_kagura_3s_kagu.kagura_sys_0012
007	¿A qué te refieres?	VO_sol.sol_sys_0027
008	La tribu de Alize que vive por aquí es la cual en la que creció Seida.	VO_kagura_3s_kagu.kagura_sys_0029
009	Son una tribu que tiene el don de "leer el viento" y percibir qué está ocurriendo a su alrededor.
010	Vas derribando todo a tu paso sin preocuparte por lo que te rodea. Me sorprendería mucho que no supiesen ya que estamos aquí.
011	¿Me estás diciendo que se han escapado? Jo. Bueno, vamos al templo ese.	VO_sol.sol_sys_0027
012	Es inútil. La tribu de Seida es muy astuta. Dudo mucho que hayan dejado ningún tipo de rastro.
013	Puede que no sea como dices.	VO_sol.sol_chara_1005
014	De hecho, hay muchas posibilidades de que se hayan dejado la Materia.
015	Escucha lo tranquilo que está todo. ¿De verdad piensas que dejarían la Materia aquí sola? Claro que no.	VO_kagura_3s_kagu.kagura_sys_0044
016	Mi argumento tiene su fundamento. Hasta ahora, cada facción siempre tenía su Materia en su templo.	VO_sol.sol_sys_0010
017	Probablemente, todos sabían que venía y, sin embargo, seguían guardándola en tan obvio lugar.
018	Puede que haya alguna razón por la que tengan que guardar la Materia en estos templos.
019	Eso es simplemente como te gustaría a ti que fuera.	VO_kagura_3s_kagu.kagura_sys_0030
020	Di lo que quieras. Vamos de todas maneras.	VO_sol.sol_sys_0017
021	No puedo perder más tiempo aquí.
022	... ...
023	Tienes que estar bromeando. Ahora que hemos llegado hasta aquí, no pienso dejar escapar esta oportunidad ahora. La encontraré a toda costa.	VO_sol.sol_sys_0016
024	El paisaje con cielo azul.
025	¿Eh?
026	Eso es lo que dijiste.	VO_kagura_3s_kagu.kagura_sys_0003
027	Me dijiste que estabas buscando un cielo como este, un paisaje con un bonito e intenso cielo azul.
028	¿Qué querías decir con eso?
029	¿Por qué me preguntas esto ahora? ¿Qué bien te puede hacer saber eso?	VO_sol.sol_sys_0027
030	¿Qué bien?	VO_kagura_3s_kagu.kagura_chara_1002
031	Si entiendo tu forma de pensar, quizá podríamos encontrar una forma de vivir juntos, ¿no?
032	Puede que haya una forma que no requiera de tantas muertes innecesarias.
033	¿Entender mi forma de pensar? Eso no es posible.	VO_sol.sol_sys_0022
034	¿Por qué?	VO_kagura_3s_kagu.kagura_chara_1013
035	No puedes pedirle a alguien que no ha escuchado música en su vida que cante una canción.	VO_sol.sol_sys_0001
036	Tú has vivido de forma pacífica en este mundo. No hay forma en que tú puedas entenderlo.
037	Por supuesto, no puedo...a menos que intentes explicármelo.	VO_kagura_3s_kagu.kagura_sys_0031
038	¿Puedes imaginarte una vida en la que tu mera existencia es un crimen?	VO_sol.sol_sys_0044
039	¿Qué?	VO_kagura_3s_kagu.kagura_sys_0025
040	Lost Blue es una ciudad construida por aquellos que han conseguido escapar de sus perseguidores de todas partes del mundo.	VO_sol.sol_sys_0010
041	Si tuviésemos que expresar nuestra existencia, aquellos que nos persiguen enseguida darían con nosotros y destruirían nuestras vidas.
042	Todos aquellos que viven allí no tienen más remedio que negar su propia existencia.
043	¡...!
044	En tal oscuro subterráneo, no hay ni océanos ni cielos bonitos.	VO_sol.sol_sys_0018
045	Como puedes suponer, en semejante ambiente, conseguir comida no es fácil. El hambre y las peleas son la constante diaria allí.
046	Lo que existe allí es envidia y resentimiento para aquellos que viven en la superficie.
047	Es un mundo sin color donde nada salvo sentimientos negativos se arremolinan por el aire.
048	Esa es la tierra en la que naciste y creciste...
049	Sí. Creo que la forma actual de existencia de Lost Blue está equivocada.	VO_sol.sol_sys_0004
050	La atmósfera de desesperación fue creada por nuestros predecesores quienes continuaron abandonando y escapando de la realidad.
051	La gente de Lost Blue ha abandonado toda esperanza. Yo quiero llevarles a la luz.
052	La luz...
053	No obstante los sueños imposibles no cambiarán nada.	VO_sol.sol_sys_0022
054	Para llevarles a la luz, lo que hace falta es ser suficientemente poderosos como para superar la fuerza de los de la superficie.
055	¿Y por eso es que, como Orion, estás buscando a los fundadores de la alquimia?	VO_kagura_3s_kagu.kagura_sys_0044
056	¿Los fundadores de la alquimia? Subestimas mi ambición.	VO_sol.sol_battle_1017
057	¿Eh?	VO_kagura_3s_kagu.kagura_sys_0025
058	Te lo preguntaré una vez más: ¿Crees que solo existe una única realidad?	VO_sol.sol_sys_0002
059	Los fundadores de la alquimia son simplemente una materialización en este pequeño mundo.
060	Este mundo es solo uno entre muchos. E igual número de realidades distintas que existen.
061	¿De qué estás hablando? ¿Quieres decir que no buscas el poder de los fundadores?	VO_kagura_3s_kagu.kagura_sys_0039
062	En efecto. El inmenso poder que yo busco...podrías decir que es incluso el origen de los fundadores de la alquimia.	VO_sol.sol_sys_0004
063	Ese poder, sin lugar a dudas, yace más allá de la torre que lleva a otra capa dimensional. Estoy seguro.
064	¿Qué torre? ¿Estás hablando de la Torre de Babel en Northern Pride?	VO_kagura_3s_kagu.kagura_sys_0031
065	No. Es parecida, pero existe en otra dimensión. Es una torre distante, que no puede verse a simple vista.	VO_sol.sol_chara_1005
066	La gente de Lost Blue llama a esta torre Veda.
067	¿Veda?	VO_kagura_3s_kagu.kagura_sys_0025
068	La punta de la torre todavía no se ha manifestado.	VO_sol.sol_sys_0022
069	Para poder hacer visible la punta y obtener así un inmenso poder,
070	hace falta un poder considerable de energía de alquimia. Una energía comparable a la de las Piedras Sagradas.
071	¿Por eso estás consiguiendo los elementos de la Materia?	VO_kagura_3s_kagu.kagura_sys_0026
072	Puedes decir eso. Aunque, con cierto tiempo, al final podría conseguir acceder a la punta.	VO_sol.sol_sys_0004
073	Pero con las Piedras Sagradas o la energía de alquimia de la Materia se puede conseguir sin perder tiempo.
074	En otras palabras, podríamos alcanzar la punta enseguida.
075	Y por eso sirves bajo las órdenes de Greed Dike en busca de las Piedras Sagradas.	VO_kagura_3s_kagu.kagura_sys_0044
076	Eh, espera un momento. Para dejar las cosas claras. No servimos bajo ellos.	VO_sol.sol_battle_1017
077	¿Eh?	VO_kagura_3s_kagu.kagura_sys_1047
078	Originalmente estábamos buscando la Piedra Sagrada de Slothstein.	VO_sol.sol_sys_0022
079	Pero no pudimos encontrarla en ninguna parte.
080	Y en ese momento se nos acercó Orion comentándonos la existencia de la Materia. Nos informó de que su poder era comparable al de las Piedras Sagradas.
081	Simplemente aceptamos esta misión bajo la condición de recibir la Materia como recompensa. Somos socios a partes iguales.
082	¿Recompensa?	VO_kagura_3s_kagu.kagura_chara_1013
083	Para empezar, la Materia no le pertenece a Greed Dike. ¡Le pertenece a las tribus de Saga!
084	Poco importa en realidad.	VO_sol.sol_sys_0007
085	Venga. Vamos al templo de viento. Si han dejado la Materia allí, enseguida se acabará todo.
086	Será el final de todos nuestros problemas.	VO_sol.sol_sys_0010
087	Así que, después de todo, esto no era más que para conseguir cumplir tus propios deseos. Como si fueses un niño.	VO_caida.caida_sys_0030
088	¿Eh?
089	¿Me estás diciendo que el capitán Monzein y los hombres y mujeres de las tribus han muerto para complacer a un niño como tú?	VO_caida.caida_sys_1016
090	¡Seida!	VO_kagura_3s_kagu.kagura_battle_0019
091	¿Así que tú eres Seida?	VO_sol.sol_sys_0044
092	Puedes disfrazar de lo que quieras tus acciones. Sea como sea, lo que tú y tu gente ha hecho es imperdonable.
093	Con este arco, vengaré a todos a los que sus vidas les fueron arrebatadas.