﻿001	Nada, ni un miserable brote, ni siquiera el intento de alguno.	VO_karla.karla_sys_0030
002	Sabía que estarías aquí. ¿Cuántas veces tengo que decirte que es imposible hacer que nada crezca aquí?	VO_balt.balt_sys_0022
003	Las cosas no se saben hasta que no se intentan. Eso es lo que solía decir Hawk, ¿te acuerdas? Nada es imposible.	VO_karla.karla_sys_0012
004	Darse por vencido antes de intentarlo es... Cómo era la palabra... ¡Ah, negligente!	VO_karla.karla_sys_0017
005	Solo digo que deberías centrarte en lo que tienes delante en lugar de depositar tu fe en algo imposible de predecir.	VO_balt.balt_sys_0031
006	No podremos cambiar nada pensando de esa manera. ¡Hawk nos confió la tarea de traer vegetación a este desierto!	VO_karla.karla_sys_0015
007	Es cierto... Supongo que tienes razón. Haz lo que tengas que hacer y te apoyaré en todo lo que pueda.	VO_balt.balt_sys_0007
008	Carla, tenemos un informe: ¡las caravanas que cruzan el desierto están desapareciendo una tras otra!	VO_balt.balt_sys_0003
009	¡¿Qué?! ¡Llévame al sitio! ¡Hazles saber que los Halcones del Desierto se encuentran en camino!	VO_karla.karla_sys_0025
010	Esta es la zona en cuestión. ¿Serán monstruos los que atacan las caravanas, u otra cosa?	VO_balt.balt_sys_0031
011	No tiene sentido especular. Tendremos que identificar a los culpables con las manos en la masa y descubrir todo lo que podamos.	VO_karla.karla_sys_0010
012	Si fuésemos un país en condiciones, probablemente podríamos lidiar con situaciones como esta con mucha más eficiencia. ¡Necesitamos leyes que motiven a la gente a ayudarse los unos a los otros!	VO_karla.karla_sys_0030
013	De ese modo, nadie tendría que arreglárselas por su cuenta..., como nosotros mismos hicimos.	VO_karla.karla_sys_0018
014	Si todos los habitantes del desierto se respaldasen entre sí, menos gente habría tenido que pasar por tragedias como aquella.
015	Es un sueño dentro de un sueño el tratar de convertir este lugar en un país. Los habitantes del desierto están demasiado ocupados con el mero hecho de garantizar su propia supervivencia. Por eso necesitan a mercenarios como nosotros.	VO_balt.balt_sys_0022
016	Estás en lo cierto, pero... nunca lo sabremos a menos que lo intentemos. ¿Eh? ¿Qué es eso?	VO_karla.karla_battle_0019
017	Hola, Halcones del Desierto. Qué coincidencia.	VO_ankh.ankh_sys_0025
018	¡Ankh! Si estás aquí, entonces...	VO_balt.balt_sys_0025
019	Oh, hola, veo que vosotros también habéis arrastrado el trasero hasta aquí.	VO_ryle.ryle_sys_0006
020	¡Qué raro se me hace ver a un perezoso como tú mancharse las manos, Ryle!	VO_balt.balt_sys_0044
021	¿A ti qué te pasa? Quedaos al margen de esto, odio tener que enfrentarme a problemas innecesarios.	VO_ryle.ryle_sys_0012
022	Estamos aquí por negocios, igual que vosotros. No recibimos órdenes vuestras.	VO_balt.balt_sys_0010
023	¿Acaso se os ha encargado venir? Probablemente habéis venido precipitadamente, pegando voces mientras hablabais sobre la paz en el desierto y cosas así.	VO_ryle.ryle_sys_0030
024	A vosotros solo os interesa el dinero, así que seguro que solo habéis venido a acabar con algo que os impide conseguir beneficios.	VO_balt.balt_sys_0022
025	En serio..., me cansa demasiado hablar con vosotros. ¿Os importa? Estamos intentando ocuparnos de las cosas con discreción. ¿Sería mucho pediros que os apartaseis de nuestro camino?	VO_ryle.ryle_sys_0029
026	Estamos aquí solo para garantizar la paz del desierto. Mientras que vuestros negocios no interfieran con ella, no tendremos ningún motivo para interferir en vuestros asuntos.	VO_karla.karla_sys_0010
027	Esta carretera es una de las principales rutas comerciales de las Alas del Desierto. Es de vital importancia que rectifiquemos esta situación o todo el negocio acabará en una gran pérdida financiera.	VO_ankh.ankh_sys_0029
028	No puedo dejar de insistir en el gran inconveniente que sería para nosotros que os entrometieseis demasiado en nuestra labor aquí.	VO_ankh.ankh_sys_0016
029	Hemos venido a salvaguardar la paz en el desierto, y vosotros habéis venido para proteger vuestros beneficios, pero todos hemos venido para averiguar el motivo de las desapariciones.	VO_karla.karla_sys_0031
030	Parece que compartimos el mismo objetivo, ¿por qué no cooperamos en esta ocasión?	VO_karla.karla_sys_0007
031	No, es demasiado esfuerzo. No me preocupa mantener la paz ni las cosas de ese tipo. La verdad es que la paz acabaría con mi negocio... Estoy teniendo un déjà vu. ¿No hemos tenido ya esta conversación?	VO_ryle.ryle_sys_0040
032	No es posible que no te preocupe, todos vivimos aquí en comunidad. Dependemos los unos de los otros, ¿no hay una parte de ti que quiera ver la paz en este lugar?	VO_karla.karla_sys_0012
033	Ya estoy lo suficientemente ocupado dirigiendo a mi gremio. Siempre estás despotricando sobre cosas para las que careces de capacidad de decisión, como construir un país, traer la paz al desierto y demás.	VO_ryle.ryle_sys_0004
034	Alguien tiene que hacerlo. Prefiero sobrepasarme en mis responsabilidades que limitarme a una pequeña esfera de influencia.	VO_karla.karla_sys_0027
035	Tienes derecho a tener tu propia opinión, pero deja de intentar hacernos comulgar con ella, por favor. Tengo mis propios métodos de hacer las cosas y te agradecería que los respetases.	VO_ryle.ryle_sys_0002
036	Aunque nos enfrentemos a la misma crisis, ¿no lucharás hombro con hombro junto a nosotros?	VO_karla.karla_battle_0019
037	Ni siquiera sabemos lo que está sucediendo aquí. No hagas suposiciones diciendo que nos estamos enfrentando a una crisis.	VO_ryle.ryle_sys_0017
038	Bueno, supongo que tiene sentido que estés sensible después de lo que le pasó a tu amigo, pero intenta que tus dramas personales no te nublen el juicio.	VO_ryle.ryle_sys_0007
039	¡Serás...!
040	Ryle, ya basta.	VO_balt.balt_sys_0014
041	¿Cómo vas con el jardín?	VO_ryle.ryle_sys_0002
042	Nada todavía.	VO_balt.balt_battle_0019
043	Me lo figuraba. ¿No tienes otras cosas de las que ocuparte en lugar de invertir tanto tiempo en un esfuerzo tan inútil como ese?	VO_ryle.ryle_sys_0007
044	La cooperación requiere un incentivo de algún tipo para que todos participen. La Zona Desierta duró tan poco porque carecía de ese incentivo.
045	Ojalá alguien pudiera encontrar un incentivo que hiciera a la gente unirse... Bueno, mejor os dejo esos temas a vosotros.	VO_ryle.ryle_sys_0016
046	Solo digo que... apelar a los sentimientos no os traerá los resultados que buscáis. Especialmente cuando estamos hablando de gremios de mercaderes como el nuestro.
047	...
048	Maestro Ryle, ¿estáis bien? Lo siento, pero parece que nos han rodeado.	VO_ankh.ankh_sys_0018
049	Carla, ¿puedes luchar?	VO_balt.balt_sys_0002
050	¡Claro que sí! ¡¿Para qué te crees que he venido?! Si no luchase, estaría siendo... ¿Cómo era la maldita palabra? ¡Negligente!	VO_karla.karla_sys_0006
051	Deben de ser los que atacan a las caravanas. ¡Los atraparemos y les haremos hablar, vaya que sí!	VO_karla.karla_sys_0010
052	Parece que ahora estamos bien envueltos en el asunto. ¿Qué os gustaría hacer, maestro Ryle?	VO_ankh.ankh_sys_0029
053	Ugh, no es que tenga ganas de hacer algo, pero parece que no tenemos alternativa: lucharemos.	VO_ryle.ryle_sys_0022