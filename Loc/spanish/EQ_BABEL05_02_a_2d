﻿001	Mi objetivo siempre ha sido convertirme en una persona de la que mi familia pudiera sentirse orgullosa.
002	Con mis armas, mis máquinas y el significado de mi color de pelo...
003	Si diera lo mejor de mí, si trabajara más que los demás... De niña... creía que bastaría con mi esfuerzo.
004	Cuando éramos niños..., mis padres se separaron antes de que yo tuviera uso de razón.
005	Yo me fui con mi padre, y mi hermano pequeño con mi madre.
006	No llego a recordar la cara de mi madre, pero sí que me acuerdo de haberla visto una vez.
007	Nunca olvidaré lo que me dijo entonces.
008	"Tienes un hermano que desea convertirse en un alquimista de renombre".
009	"Estoy segura de que se convertirá en un gran paladín y en un alquimista excepcional. Te enorgullecerás de él".
010	"Como su hermana que eres, tú también tendrás que dar lo mejor de ti para que él pueda sentirse igual de orgulloso".
011	Cuando lo pienso, no me queda claro si me dijo todo eso llevada por una especie de cariño materno extraviado, si intentaba ser educada, o qué.
012	Aunque no creo que llegue a enterarme nunca porque ya murió.
013	Desde entonces, he oído que mi hermano se fue de la Guardia Sagrada y que ha abandonado la idea de ser alquimista.
014	Pero, aun así..., nunca he podido olvidar las palabras de mi madre.
015	En cuanto a mi hermano, no sé cómo se llama ni qué aspecto tiene, y mucho menos dónde se encuentra...
016	Aun así, siento como si llevase toda la vida esperándolo.
017	Volveré pronto, padre.
018	[Muchas gracias por vuestro apoyo constante.]
019	[Siento mucho las molestias, pero hay algo que debo hacer.]
020	[Por lo tanto, lamento informarles de que la tienda permanecerá cerrada indefinidamente. Firmado: Albea.]
021	Tengo un sitio al que volver.
022	Mi padre, quien me enseñó todo lo que sé en materia de mecánica...
023	... era un hombre anticuado y terco que nunca llevaría esta máscara con la que yo cargo.
024	Siempre me vio como una niña, independientemente de la edad que tuviera.
025	Sin embargo, al final de su vida, pudo reconocerme como una persona independiente. Le costó lo suyo, a pesar de que ya llevaba un tiempo encargándome de más pedidos que él en la tienda.
026	El humo y los vapores, gajes de nuestro oficio, fueron los que finalmente acabaron con él.
027	Me dejó su taller, repleto de todas sus invenciones..., incluido el lugar vacío en el que solía ponerse a trabajar.
028	Ninguno de los dos fuimos nunca buenos en organizar las cosas, y parecía que el caos nos consumiría en cualquier momento.
029	Solía decirme que su sueño era conseguir que las máquinas se encargaran de ese tipo de trabajo.
030	También me dijo que no se arrepentía de confiarme su taller a mí.
031	Al final, me dejó salir al mundo.
032	He conseguido crear un hogar, hermano. Tienes un sitio al que volver.
033	Si no averiguo nada sobre mi hermano, no podré traerlo a casa.
034	Supongo que será tan cabezota como yo.
035	¡Q-qué historia tan emotiva! Aaaains...
036	¡No llores!	VO_wilhelm.wilhelm_sys_0012
037	¡Buuarf!
038	¡No me vomites encima!	VO_wilhelm.wilhelm_sys_0027
039	¡Yo estoy en la misma situación, mi hermano pequeño es la única familia que me queda! ¡Creo que sé cómo te sientes!
040	Así que llevabas una tienda, ¿eh? Supongo que eso explica la cantidad de cacharros que llevas encima.	VO_wilhelm.wilhelm_sys_0044
041	Puede que me sean útiles en algún momento. Me han dicho que el mundo exterior es peligroso.	VO_albell.albell_sys_0037
042	He traído todo lo que he podido.
043	¡Estupendo! Después de todo, no tiene sentido perder el tiempo empacando. ¡Mucho mejor meter todo lo que tengas delante en una maleta y ponerte en marcha!
044	Exacto, la preparación es importante.	VO_albell.albell_sys_0004
045	¡Sip, tú lo has dicho!
046	Parece que tenéis muchas cosas en común.	VO_wilhelm.wilhelm_sys_0010
047	Je, je, je.
048	He decidido que no voy a volver a casa sin mi hermano.	VO_albell.albell_sys_0016
049	Quiero preguntarle por qué abandonó su sueño de convertirse en alquimista.
050	Tienes que entender el mecanismo de una máquina para entender qué ocurre cuando se avería.
051	Puede que tenga que estudiar alquimia para entender a mi hermano.
052	Al menos, eso es lo que creo.	VO_albell.albell_sys_0030
053	Laboratorio de investigación de Slothstein
054	Te han dado la espalda.	VO_wilhelm.wilhelm_sys_0045
055	Sí, pero oí a los investigadores hablar de que hace poco habían descubierto las ruinas de una antigua civilización de Babel que tiene algún tipo de conexión con la alquimia.	VO_albell.albell_sys_0004
056	La gente que fue a investigar el lugar desapareció, así que les dijeron que no se acercaran.
057	Y aun así estás aquí.
058	Para resolver un problema, hay que ir hasta el fondo del asunto.	VO_albell.albell_sys_0037
059	¡Impresionante! Veo que para ti es pan comido escuchar a hurtadillas a investigadores incautos.	VO_wilhelm.wilhelm_sys_0030
060	Bueno, no es tan fácil.	VO_albell.albell_battle_1015
061	Vaya, un mecabot en miniatura con alas. ¡Y vuela!
062	¡Guau, increíble!
063	No es nada difícil de crear.	VO_albell.albell_sys_0004
064	¡Bueno, bueno!
065	¿Q-qué pasa?	VO_albell.albell_sys_0025
066	¡Oye, oye! ¡¿Y crees que yo también podría fabricar uno?!
067	No sabía que a las chicas les gustase este tipo de cosas.	VO_albell.albell_sys_0044
068	¡Pues claro que sí, tontaina! ¡Es alucinante!
069	¿E-en serio lo crees?	VO_albell.albell_sys_0025
070	Bueno, entonces, solamente necesitamos los planos y los materiales necesarios.
071	¡¿Planos?! ¡Déjame ver! ¡Quiero probar!
072	Va-vale.	VO_albell.albell_sys_0004
073	Aaaay... No puedo hacerlo.
074	Je, je, eres una chica rara.	VO_albell.albell_sys_0007
075	¿Y por qué no nos las apañamos con el que ya está hecho?	VO_wilhelm.wilhelm_sys_0002
076	A ver, el lugar está muy bien custodiado. No podremos reunir demasiada información si nos limitamos a escuchar a escondidas.	VO_albell.albell_sys_0031
077	Si alguien te contara todo lo que quieres saber sobre alquimia, ¿abandonarías las ruinas?	VO_wilhelm.wilhelm_sys_0045
078	¿Qué clase de conexión tienes tú con las ruinas?	VO_albell.albell_sys_0002
079	No tengo por qué contestarte a eso.	VO_wilhelm.wilhelm_sys_0010
080	Entonces no tengo por qué irme, ¿a que no? No pienso hacer promesas.	VO_albell.albell_sys_0044
081	Eres una niñata, ¿lo sabías?	VO_wilhelm.wilhelm_sys_0030
082	¡¿Perdona?!
083	¡Ajá!
084	Hay líneas que no deberías cruzar.	VO_wilhelm.wilhelm_sys_0014
085	¡Mira quién fue a hablar! ¡Me parece que eres tú quien acaba de cruzarla!	VO_albell.albell_sys_0029
086	¿Y qué línea he cruzado yo?	VO_wilhelm.wilhelm_sys_0004
087	¡¿Que qué línea...?!
088	¡Para el carro! Llevas manoseándote el pelo desde que empezamos a hablar. ¡Eres una niñata repelente!	VO_wilhelm.wilhelm_sys_0030
089	¡Ahí está! ¡A esa línea me refiero!	VO_albell.albell_sys_0013
090	Deja ya de lloriquear... ¡Y baja la voz!	VO_wilhelm.wilhelm_sys_0004
091	¡Puf, eres insoportable!	VO_albell.albell_sys_0029
092	Por cierto, ¿te sueles lavar?	VO_wilhelm.wilhelm_sys_0002
093	¡¿Que qué?! ¿Me acabas de pre...? ¡¿Qué?!	VO_albell.albell_sys_0026
094	No digo que huelas mal ni nada parecido.	VO_wilhelm.wilhelm_sys_0007
095	¡Se-serás...! No... ¡No tengo por qué aguantar esto!	VO_albell.albell_sys_0025
096	Relaja la raja.
097	¡Ya está bien! ¡Iré por mi cuenta!	VO_albell.albell_sys_0013
098	Anda... ¡No seas así!
099	Quítate de en medio, Mira.	VO_albell.albell_sys_0029
100	¡¿Eh?!
101	Un segundo. No puedes tocarme.
102	A-acabo de atravesarte... con la mano. ¡¿Qué está pasando aquí?!	VO_albell.albell_sys_0027
103	¡¿Qué demonios?!	VO_wilhelm.wilhelm_sys_0025
104	¡Vamos a hacer un trato!
105	A ver..., ahora que lo mencionas, tengo que echarle un vistazo a una cosa. Creo que iré contigo.	VO_wilhelm.wilhelm_sys_1016
106	¡Ni lo sueñes!	VO_albell.albell_sys_0029
107	¡Sí que lo sueño!
108	Madre mía.
109	¡Vaya, don Will! ¡Qué placer tan inesperado!
110	Puaj, no me puedo creer que de verdad lo llamen don Will.	VO_albell.albell_sys_0026
111	De verdad, es repulsivo, ¿eh? ¡Buaj!
112	Je, je, la verdad es que sí.
113	Tú también me puedes llamar don Will, ¿eh?	VO_wilhelm.wilhelm_sys_0007
114	Sigue soñando.	VO_albell.albell_sys_0040
115	¿Qué podemos hacer por usted, don Will?
116	Necesito que me hagáis un hueco en vuestro equipo de investigación. ¿Os importa que me una?	VO_wilhelm.wilhelm_sys_0037
117	¡Claro que no! Estamos en deuda con usted después de..., bueno, ya sabe...
118	Mejor hablemos de eso en otro momento, ¿de acuerdo?	VO_wilhelm.wilhelm_sys_1020
119	¡Por supuesto! Cuando usted quiera.
120	No puedo creer que me hayan dado la espalda sin escuchar ni una palabra de lo que tengo que decirles.	VO_albell.albell_sys_0029
121	Tener a gente en deuda contigo siempre ayuda.	VO_wilhelm.wilhelm_sys_0007
122	¿Y qué pasa con todo este asunto?	VO_albell.albell_sys_0002
123	Siempre hay cosas del pasado que la gente prefiere ocultar.	VO_wilhelm.wilhelm_sys_0001
124	La verdad es que esa persona parecía tenerte en muy alta estima.	VO_albell.albell_sys_0044
125	Ya sabes el dicho: todos somos hermanos.	VO_wilhelm.wilhelm_sys_1016
126	Eso no responde a mi pregunta.	VO_albell.albell_sys_0003
127	Bueno, quizás sea porque no tengo intención de responderte.	VO_wilhelm.wilhelm_sys_0007
128	Puf. ¿Eso qué es?	VO_albell.albell_sys_0005
129	¡No me digas que nunca has visto una baraja de cartas!	VO_wilhelm.wilhelm_sys_0004
130	Pues claro que sí.	VO_albell.albell_sys_0039
131	En una baraja hay cincuenta y dos cartas divididas en cuatro palos.	VO_wilhelm.wilhelm_sys_0037
132	¡Ya lo sé!	VO_albell.albell_sys_0039
133	Es que me he imaginado que probablemente nunca has tenido amigos con los que jugar a este tipo de cosas, ya que parece que te pasas el día tonteando con tus maquinitas.	VO_wilhelm.wilhelm_sys_0044
134	¿Te estás...? ¿Te estás riendo de mí?	VO_albell.albell_sys_0013
135	Pues claro que sí.	VO_wilhelm.wilhelm_sys_0007
136	¡Ja, ja, ja, ja, ja!
137	¡Pues la verdad es que no lo entiendo!
138	¿Eh?	VO_wilhelm.wilhelm_sys_0004
139	¿Has dicho que esto es una baraja de tartas?
140	Una baraja de cartas, no de tartas.	VO_wilhelm.wilhelm_sys_0001
141	Ah, ¿una rebaja de tartas? ¿Dónde?
142	¿Pero qué puñetas dices?	VO_wilhelm.wilhelm_sys_0030
143	¿En la pastelería de la calle Decimocarta?
144	¿Cuánto tiempo te vas a pasar así? ¡¿Lo haces a propósito?!	VO_wilhelm.wilhelm_sys_0012
145	Bueno, como te decía... En una baraja hay cincuenta y dos cartas divididas en cuatro palos...
146	Y para jugar hay que cortar la baraja, ¿no?
147	¡No la cortes! ¡Que se rompe!	VO_wilhelm.wilhelm_sys_0025
148	"Cortarla" significa mezclarla, Mira. Se mezcla así.	VO_albell.albell_sys_0003
149	Vamos a echar una ronda de Culo sucio.	VO_wilhelm.wilhelm_sys_0037
150	¿Por qué?	VO_albell.albell_sys_0039
151	¿Es que no te aburres? Estás aquí encerrada en esta vieja biblioteca esperando recibir lecciones de alquimia.	VO_wilhelm.wilhelm_sys_0004
152	Anda, venga, date prisa y repártelas.	VO_albell.albell_sys_0017
153	Entonces, todos tenemos que tener las cartas en la mano, ¿no?
154	¿Tú también juegas, Mira?	VO_albell.albell_sys_0004
155	No, yo creo que paso.
156	¡¿Que qué?! ¡Entonces solo somos dos!	VO_albell.albell_sys_0025
157	No puedo tocar las cosas, ¿recuerdas?
158	¡Bu-bueno, yo...!	VO_albell.albell_sys_0013
159	Empezaré hablando sobre la alquimia de Slothstein.	VO_wilhelm.wilhelm_sys_1020
160	¿Vas a empezar ahora?	VO_albell.albell_sys_0021
161	Abre ese libro de ahí por la página cien.	VO_wilhelm.wilhelm_sys_0024
162	¡No puedo jugar a las cartas si tengo que leer!	VO_albell.albell_sys_0026
163	Descártate las parejas, rapidito.	VO_wilhelm.wilhelm_sys_0007
164	¡Ay! ¡E-espera! ¡No puedo...!	VO_albell.albell_sys_0027
165	Este país aprecia la eficiencia por encima de todo. Sin embargo, la obsesión por la eficiencia a veces puede desembocar en pereza.	VO_wilhelm.wilhelm_sys_0031
166	Uhhh, ¡directo al grano!
167	A través de la aplicación de la alquimia, hemos mejorado nuestros motores de vapor y nuestras máquinas.
168	Y no solo eso, sino que también hemos usado la alquimia para esclavizar a los demonios poniéndolos a trabajar con la intención de que nuestra isla se desarrolle más rápido. Necesitamos su ayuda.
169	¿De-demonios...?	VO_albell.albell_sys_0005
170	Eh, es tu turno. Roba una carta.	VO_wilhelm.wilhelm_sys_0012
171	Aaj, ¡vale!	VO_albell.albell_sys_0029
172	Se dice que estos demonios son un arma ancestral, fabricada en secreto con una antigua y poderosa alquimia.
173	...
174	Según la historia, tal y como la registró la Orden Sagrada, los demonios redujeron la antigua civilización de Babel a cenizas en un instante.
175	Si se volvieran a enemistar con la humanidad, solo la alquimia podría detenerlos.
176	Bueno, quizás no bastase solo con la alquimia.	VO_wilhelm.wilhelm_sys_0039
177	Nunca he visto nada igual.	VO_albell.albell_sys_0031
178	Está custodiada bajo llave, por supuesto. Es alto secreto y todo eso, siempre con el respaldo de la Guardia Sagrada.	VO_wilhelm.wilhelm_sys_0037
179	Los demonios se crearon de tal forma que pierden su poder en cuanto intentan abandonar el país. Así, no pueden infringir el preciado orden del Código Magno.
180	Así es como Slothstein se ha valido de la alquimia para mejorar la eficiencia interna y progresar espectacularmente como sociedad.
181	Hurra por el uso de armas destructivas para lograr un mundo en el que imperen la paz y la seguridad..., supongo.
182	¿Ahora entiendes por qué no se te permite la entrada a este lugar?	VO_wilhelm.wilhelm_sys_0007
183	Yo...
184	¡Je, je! He ganado.
185	¡Ay!	VO_albell.albell_battle_1018
186	Puedes quedártelo.	VO_wilhelm.wilhelm_sys_0002
187	¡¿Có-cómo?!	VO_albell.albell_sys_0025
188	El comodín. Considéralo un regalo.
189	En cualquier caso, así es la naturaleza de la alquimia. Siempre ha habido quienes la han usado para fines egoístas y han traído al mundo todo tipo de calamidades.
190	Por eso se considera un arte prohibido que solo puede usar la Orden Sagrada, que cumple rigurosamente con lo que se conoce como el Código del Alquimista.
191	...
192	(La alquimia se creó en primer lugar para derrotar a las bestias malditas.)
193	(Sin embargo, el arte ha ido cambiando con el paso del tiempo.)
194	(Ha sido una amenaza para la humanidad, una herramienta usada para destruir civilizaciones enteras.)
195	(¿Y eso por qué?)
196	Mi hermano formaba parte de la Guardia Sagrada.	VO_albell.albell_sys_0002
197	Era un buen muchacho.	VO_wilhelm.wilhelm_sys_0044
198	Los de la Guardia Sagrada lo pescaron poco después de unirse al ejército de Wratharis.	VO_albell.albell_sys_0037
199	Se le consideraba un genio como alquimista.
200	...
201	Un genio... Bueno, un genio exalquimista que nació en Slothstein y que vive en Wratharis. Presiento que debe haber alguien en el campo que lo conozca.	VO_albell.albell_sys_0002
202	¿Te importaría preguntarles a los del laboratorio?
203	Puuuf.	VO_wilhelm.wilhelm_sys_0007
204	¿En serio tengo pinta de ir haciendo favores?	VO_wilhelm.wilhelm_sys_0002
205	La verdad es que no.	VO_albell.albell_sys_0039
206	Ya he terminado mi investigación. Incluso te he dado una clase gratis, así que me parece que ya he hecho más que suficiente.	VO_wilhelm.wilhelm_sys_0030
207	Si te empeñas en ir por esa vía, acabarán matándote.
208	Venga, hombre.	VO_albell.albell_sys_0044
209	Te lo digo en serio. Es una advertencia.	VO_wilhelm.wilhelm_sys_0004
210	¿Y quién va a matarme, eh? ¿Serás tú? ¿U otra persona?	VO_albell.albell_sys_0002
211	...
212	Deja que adivine... ¿Lo dices por mi propio bien?	VO_albell.albell_sys_0010
213	Así es.	VO_wilhelm.wilhelm_sys_0037
214	Entonces creo que no tengo nada que perder.	VO_albell.albell_sys_0031
215	¿Adónde vas?	VO_wilhelm.wilhelm_sys_0004
216	Voy a ir yo sola a echar un vistazo.	VO_albell.albell_sys_0016
217	Sigue persiguiendo la alquimia tanto como quieras, no te llevará a ningún lado.	VO_wilhelm.wilhelm_sys_0001
218	No voy a rendirme solo porque sea peligroso. Estoy hecha de una buena pasta como para aguantar eso y mucho más.	VO_albell.albell_sys_0039
219	Aunque temiera que me mataran y decidiera abandonar mi objetivo para volver a mi tienda...
220	Estaría... sola.
221	...
222	No quiero... No quiero estar sola.	VO_albell.albell_sys_0022
223	...
224	Um... Ya está.	VO_albell.albell_sys_0030
225	¿Eh? ¿Qué haces en el suelo?	VO_wilhelm.wilhelm_sys_0004
226	Acelerador...	VO_albell.albell_sys_0004
227	¿A ti qué te pasa? ¿Por qué pulsas esos botones en tus botas?
228	Adiós.	VO_albell.albell_sys_0010
229	¡Ey! ¡Pero qué...!	VO_wilhelm.wilhelm_sys_0044
230	¡Vaya! ¡Va echando leches!
231	Supongo que tendremos que seguirla a pie.	VO_wilhelm.wilhelm_sys_0029
232	¿Por qué tenemos que seguirla?
233	Porque sí.	VO_wilhelm.wilhelm_sys_0004
234	Creía que tú no eras de los que hacían favores.
235	¿La has perdido de vista?
236	Sí, hace un rato ya.	VO_wilhelm.wilhelm_sys_0037
237	¿Eh? ¿Por qué lo preguntas?
238	Eso de quedarte y esperar para atacarnos cuando llegáramos a una zona desierta... Sabes lo que te haces.	VO_wilhelm.wilhelm_sys_0001
239	Los que se acerquen a las bestias malditas deben ser eliminados.	VO_stknightm.stknightm_sys_0007
240	Para que lo sepas, esa chica que se ha ido corriendo no sabe nada de ese asunto.	VO_wilhelm.wilhelm_sys_0012
241	Yo soy el único que sabe lo que se cuece.
242	También soy el único que tiene esta piedra... ¿Lo ves?
243	¡Eso es...!
244	No nos hace falta más gente cruzando líneas que nadie debería. Ya estoy yo para eso.